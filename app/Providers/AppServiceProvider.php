<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//use Memcached;
use Config;
use Memcache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Memcache::class, function ($app){
            $object = new Memcache();
            $object->addServer(Config::get('cache.memcached.servers'));
            return $object;
        });
    }
}

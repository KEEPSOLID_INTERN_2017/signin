<?php

namespace App\Http\Controllers\Api;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Memcache;

class SessionController extends Controller
{
    public function checkSession(Request $request, Memcache $memcache)
    {
        /*/session/prolong:
    post:
      tags:
        - 'Sign-In API'
      summary: 'Prolong a user''s session'
      parameters:
        - name: 'SESSION-ID'
          type: string
          in: header
          description: 'A session key retrieved when authorized'
      responses:
        200:
          description: 'Session prolonged'
          schema:
            $ref: '#/definitions/Session'
        401:
          description: 'Authorization Required'
          schema:
            $ref: '#/definitions/Message'
        403:
          description: 'Access Forbidden'
          schema:
            $ref: '#/definitions/Message' */

        $session = $request->header('SESSION-ID');
        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);
            $token = substr($token, strlen(env('SECRET_KEY', 'vM7YVROL4QcCPDZGLKobmDZA')));

            $sessionInfo = $memcache->get($token);
            if (!$sessionInfo) {
                return (new Response([
                    'code' => 401,
                    'message' => 'Session expired'
                ], 401, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => 403,
                'message' => 'Incorrect session key received'
            ], 403, ['description' => 'Access Forbidden']));
        }

        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => 401,
                'message' => 'Session expired'
            ], 401, ['description' => 'Authorization Required']));
        }

        if ($sessionInfo[2] < time() + config('session.lifetime') * 30){
            $expire = config('session.lifetime') * 60;
            $sessionInfo[2] = time() + $expire;
            if ($memcache->set($token,$sessionInfo,false,$expire)) {
                return (new Response([
                    'id' => $sessionInfo[0],
                    'session' => $sessionInfo[1],
                    'expires_at' => $sessionInfo[2]
                ], 200, ['description' => 'Session checked and prolonged']));
            }
            return (new Response([
                'code' => 403,
                'message' => 'Session checked and not prolonged'
            ], 403, ['description' => 'Access Forbidden']));
        }

        return (new Response([
            'id' => $sessionInfo[0],
            'session' => $sessionInfo[1],
            'expires_at' => $sessionInfo[2]
        ], 200, ['description' => 'Session checked']));
    }

    public function expire(Request $request, Memcache $memcache)
    {
        /*
         session/expire:
    post:
      tags:
        - 'Sign-In API'
      summary: 'Expiring a user''s session, a.k.a. logout'
      parameters:
        - name: 'SESSION-ID'
          type: string
          in: header
          description: 'A session key retrieved when authorized'
      responses:
        200:
          description: 'Session were expired'
          schema:
            $ref: '#/definitions/Message'
        401:
          description: 'Authorization Required'
          schema:
            $ref: '#/definitions/Message'
        403:
          description: 'Access Forbidden'
          schema:
            $ref: '#/definitions/Message'
     Message:
    type: object
    properties:
      code:
        type: integer
      message:
        type: string
        */

        $session = $request->header('SESSION-ID');

        try {
            $token = \Illuminate\Support\Facades\Crypt::decrypt($session);

            $token = substr($token, strlen(env('SECRET_KEY', 'vM7YVROL4QcCPDZGLKobmDZA')));

            $sessionInfo = $memcache->get($token);

            if (!$sessionInfo) {
                return (new Response([
                    'code' => 401,
                    'message' => 'Session expired'
                ], 401, ['description' => 'Authorization Required']));
            }
        }
        catch(DecryptException $e){
            return (new Response([
                'code' => 403,
                'message' => 'Incorrect session key received'
            ], 403, ['description' => 'Access Forbidden']));
        }
        if ($sessionInfo[1] !== $session) {
            return (new Response([
                'code' => 401,
                'message' => 'Session expired'
            ], 401, ['description' => 'Authorization Required']));
        }

        if ($memcache->delete($token)) {
            return (new Response([
                'code' => 200,
                'message' => 'Session were expired'
            ], 200, ['description' => 'Session were expired']));
        }

        return (new Response([
            'code' => 403,
            'message' => 'Session not expired'
        ], 403, ['description' => 'Access Forbidden']));
    }
}

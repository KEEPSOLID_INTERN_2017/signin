<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/keepsolid2017/signin', function () {
    return \Response::make('///////keepsolid2017/signin//////////message',401);
});

$router->group(['prefix' => '/keepsolid2017/signin/v1/session'], function ($router) {
    // Работа с сессиями пользователей...
    $router->post('/expire', 'Api\SessionController@expire');
    $router->post('/check/session', 'Api\SessionController@checkSession');
});

